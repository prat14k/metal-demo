//
//  Shader.metal
//  METALDemo
//
//  Created by Prateek Sharma on 6/20/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

/*
 PARAMS
 * vertices: an array of the vertices
 * vertexID: the id of the vertex that is being processed
 
 OUTPUT
 * The output will be the input for the next stage in the pipeline
*/
struct Constants {
    float animateBy;
};

vertex float4 vertex_shader(const device packed_float3 *vertices [[buffer(0)]],
                            constant Constants &constants [[buffer(1)]],
                            uint vertexID [[vertex_id]]) {
    float4 position = float4(vertices[vertexID], 1);
    position.x += constants.animateBy;
    return position;
}

fragment half4 fragment_shader() {
    return half4(1, 0, 0, 1);
}
