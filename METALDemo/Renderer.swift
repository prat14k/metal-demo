//
//  Renderer.swift
//  METALDemo
//
//  Created by Prateek Sharma on 6/20/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import Foundation
import MetalKit

class Renderer: NSObject {
    
    struct Constants {
        var animateBy: Float = 0.0
    }
    private var constants = Constants()
    private var time: Float = 0.0
    private let metalDevice: MTLDevice?
    private let commandQueue: MTLCommandQueue?
    /*
     Vertices for the triangle
     [x, y , z]
     */
    private var vertices: [Float] = [
        -1, 0, 0,
        -1, -1, 0,
        1, 0, 0,
        1, -1, 0,
    ]
    
    private var indices: [UInt16] = [0, 1, 2, 2, 3, 1]
    
    private var pipelineState: MTLRenderPipelineState?
    private var vertexBuffer: MTLBuffer?
    private var indicesBuffer: MTLBuffer?
    
    init(device: MTLDevice? = MTLCreateSystemDefaultDevice(), mtkView: MTKView) {
        metalDevice = device
        mtkView.device = device
        commandQueue = metalDevice?.makeCommandQueue()
        super.init()
        mtkView.delegate = self
        buildModel()
        buildPipelineState()
    }
    
    private func buildPipelineState() {
        let library = metalDevice?.makeDefaultLibrary()
        let vertexFunc = library?.makeFunction(name: "vertex_shader")
        let fragmentFunc = library?.makeFunction(name: "fragment_shader")
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunc
        pipelineDescriptor.fragmentFunction = fragmentFunc
        pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        do {
            pipelineState = try metalDevice?.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            print(error)
        }
    }
    
    private func buildModel() {
        vertexBuffer = metalDevice?.makeBuffer(bytes: vertices, length: vertices.count * MemoryLayout<Float>.size, options: [])
        indicesBuffer = metalDevice?.makeBuffer(bytes: indices, length: indices.count * MemoryLayout<UInt16>.size, options: [])
    }
}

extension Renderer: MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) { }
    
    func draw(in view: MTKView) {
        guard let renderDescriptor = view.currentRenderPassDescriptor,
            let drawable = view.currentDrawable,
            let pipelineState = pipelineState,
            let indicesBuffer = indicesBuffer
            else { return }
        
        time += 1 / Float(view.preferredFramesPerSecond)
        constants.animateBy = abs(sin(time) / 2 + 0.5)
        
        let commandBuffer = commandQueue?.makeCommandBuffer()
        let commandEncoder = commandBuffer?.makeRenderCommandEncoder(descriptor: renderDescriptor)
        commandEncoder?.setRenderPipelineState(pipelineState)
        commandEncoder?.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        commandEncoder?.setVertexBytes(&constants, length: MemoryLayout<Constants>.stride, index: 1)
        commandEncoder?.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: vertices.count)
        commandEncoder?.drawIndexedPrimitives(type: .triangle, indexCount: indices.count, indexType: .uint16, indexBuffer: indicesBuffer, indexBufferOffset: 0)
        commandEncoder?.endEncoding()
        commandBuffer?.present(drawable)
        commandBuffer?.commit()
    }
}
