//
//  ViewController.swift
//  METALDemo
//
//  Created by Prateek Sharma on 6/20/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit
import MetalKit

class ViewController: UIViewController {

    private var renderer: Renderer?
    private var metalView: MTKView {
        return view as! MTKView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        renderer = Renderer(mtkView: metalView)
        metalView.clearColor = MTLClearColor(red: 0, green: 0.4, blue: 0.8, alpha: 1)
    }
}

